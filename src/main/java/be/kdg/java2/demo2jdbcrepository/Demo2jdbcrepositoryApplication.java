package be.kdg.java2.demo2jdbcrepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Demo2jdbcrepositoryApplication implements CommandLineRunner {
    @Autowired
    private PersonRepository personRepository;

    public static void main(String[] args) {
        SpringApplication.run(Demo2jdbcrepositoryApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        personRepository.findByFirstName("JACK").forEach(System.out::println);
    }
}
