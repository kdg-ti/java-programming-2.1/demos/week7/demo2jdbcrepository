package be.kdg.java2.demo2jdbcrepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Repository
public class PersonRepository {
    private static final Logger log = LoggerFactory.getLogger(PersonRepository.class);

    public List<Person> findByFirstName(String firstName){
        List<Person> persons = new ArrayList<>();
        try {
            Connection connection = DriverManager.getConnection("jdbc:hsqldb:file:dbData/demo", "sa", "");
            Statement statement = connection.createStatement();
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM PERSONS WHERE FIRSTNAME = ?");
            preparedStatement.setString(1, firstName);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                log.info("Found a person:");
                log.info("Id:" + resultSet.getInt("id"));
                log.info("Name:" + resultSet.getString("name"));
                log.info("Firstname:" + resultSet.getString("firstname"));
                persons.add(new Person(resultSet.getInt("id"),
                        resultSet.getString("name"),resultSet.getString("firstname")));
            }
            resultSet.close();
            statement.close();
            connection.close();
        } catch (SQLException sqlException) {
            log.error(sqlException.getMessage(), sqlException);
        }
        return persons;
    }

}
